package be.kriscoolen.opdracht3;


import java.util.stream.Stream;

public class StreamToArray {

    public static void main(String[] args) {
        Person person1 = new Person("Kris","Coolen", Gender.M,34,94,1.78f);
        Person person2 = new Person("Omar","Schultink", Gender.M,35,75,1.77f);
        Person person3 = new Person("Ann-Sophie","Coolen", Gender.V,3,15,1.02f);

        Stream<Person> stream = Stream.of(person1,person2,person3);

        Person[] array = stream.toArray(Person[]::new);
        for(Person p: array){
            System.out.println(p);
        }
    }
}
