package be.kriscoolen.opdracht1.deel2;

import java.util.stream.IntStream;


public class PrintIntStreamApp {
    public static void main(String[] args) {
        IntStream iStream = IntStream.range(0,100);
        //alternatief
        //IntStream iStream = IntStream.rangeClosed(0,99);
        iStream.forEach(System.out::println);
    }

}
