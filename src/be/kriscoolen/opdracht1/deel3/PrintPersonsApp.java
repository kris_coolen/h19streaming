package be.kriscoolen.opdracht1.deel3;


import java.util.stream.Stream;

public class PrintPersonsApp {
    public static void main(String[] args) {
        Person person1 = new Person("Kris","Coolen",Gender.M,34,94,1.78f);
        Person person2 = new Person("Omar","Schultink",Gender.M,35,75,1.77f);
        Person person3 = new Person("Ann-Sophie","Coolen",Gender.V,3,15,1.02f);
        Person[] persons={person1,person2,person3};
        //system.out.println(persons);
        Stream.of(persons).forEach(System.out::println);
        //alternatief (iets langer uitgeschreven)
        Stream.of(persons).forEach((Person p)-> System.out.println(p));

        //druk bmi af van alle personene in persons array
        Stream.of(persons).forEach((Person p)->System.out.println(BmiUtil.getBmi(p)));
    }
}
