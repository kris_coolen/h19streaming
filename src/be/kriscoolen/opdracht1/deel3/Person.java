package be.kriscoolen.opdracht1.deel3;

public class Person {

   private String voornaam;
   private String achternaam;
   private Gender geslacht;
   private int leeftijd;
   private float gewicht;
   private float lengte;

    public Person(String voornaam, String achternaam, Gender geslacht, int leeftijd, float gewicht, float lengte) {
        this.voornaam = voornaam;
        this.achternaam = achternaam;
        this.geslacht = geslacht;
        this.leeftijd = leeftijd;
        this.gewicht = gewicht;
        this.lengte = lengte;
    }

    public float getGewicht() {
        return gewicht;
    }

    public float getLengte() {
        return lengte;
    }

    @Override
    public String toString() {
        return "\nPerson\n" +
                "\tvoornaam='" + voornaam + "\'"+
                "\n\tachternaam='" + achternaam + "\'"+
                "\n\tgeslacht=" + geslacht +
                "\n\tleeftijd=" + leeftijd + " jaar" +
                "\n\tgewicht=" + gewicht + " kilogram" +
                "\n\tlengte=" + lengte + " meter" +
                "\n";
    }


}
