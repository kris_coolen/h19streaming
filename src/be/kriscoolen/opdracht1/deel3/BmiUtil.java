package be.kriscoolen.opdracht1.deel3;

public interface BmiUtil {

    public static float getBmi(Person p){
        float gewicht = p.getGewicht();
        float lengte = p.getLengte();
        return gewicht/(lengte*lengte);
    }
}
