package be.kriscoolen.opdracht1.deel1;

import java.util.stream.Stream;

public class PrintArrayStreamApp {
    public static void main(String[] args) {
        //String[] myWords={"Hello","this","is","an","example","of","streams"};
        String[] myWords = "Hello this is an example of streams".split(" ");
        Stream.of(myWords).forEach(System.out::println);
    }
}
