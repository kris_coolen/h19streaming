package be.kriscoolen.opdracht2.deel2;

import java.util.stream.Stream;

public class App {

    public static void main(String[] args) {
        String[] words = "Hello this is an example of streams".split(" ");
        String res = Stream.of(words).reduce(";",(acc,el)->acc+el+";");
        System.out.println(res);
    }

}
