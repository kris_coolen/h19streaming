package be.kriscoolen.opdracht2.deel1;

import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.stream.IntStream;

public class App {

    public static void main(String[] args) {

        int[] numbers = {1,5,7,12,4,5};

        long count = IntStream.of(numbers).count();
        System.out.println("Het aantal getallen = " + count);

        OptionalInt max = IntStream.of(numbers).max();
        if(max.isPresent()) System.out.println("Het maximum = " +max.getAsInt());
        else System.out.println("No maximum");

        OptionalInt min = IntStream.of(numbers).min();
        if(min.isPresent()) System.out.println("Het minimum = " + min.getAsInt());
        else System.out.println("No minimum");

        OptionalDouble av = IntStream.of(numbers).average();
        if(av.isPresent()) System.out.format("Het gemiddelde = %.2f\n",av.getAsDouble());
        else System.out.println("No average");

        System.out.println("De som = " +IntStream.of(numbers).sum());

        int result = IntStream.of(numbers).reduce(0,(acc,el) ->acc+el*el);
        System.out.println("De som van de kwadraten = " + result);



    }




}
