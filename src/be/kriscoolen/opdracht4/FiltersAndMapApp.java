package be.kriscoolen.opdracht4;
import java.util.Random;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class FiltersAndMapApp {

    public static void main(String[] args) {
        Person person1 = new Person("Kris","Coolen", Gender.M,34,94,1.78f);
        Person person2 = new Person("Omar","Schultink", Gender.M,35,75,1.77f);
        Person person3 = new Person("Ann-Sophie","Coolen", Gender.V,3,15,1.02f);
        Person person4 = new Person("Hannelore","Coolen",Gender.V,6,20,1.30f);

        Person[] persons={person1,person2,person3,person4};

        System.out.println("Alle personen die geslacht = vrouw hebben:");
        Stream.of(persons).filter(p->p.getGeslacht()==Gender.V).forEach(System.out::println);

        System.out.println("Alle personen die ouder zijn dan 30 jaar:");
        Stream.of(persons).filter(p->p.getLeeftijd()>30).forEach(System.out::println);

        System.out.println("Alle personen die tussen de 2 en 10 jaar oud zijn (inclusief): ");
        Stream.of(persons).filter(p->p.getLeeftijd()>=2).filter(p->p.getLeeftijd()<=10).forEach(System.out::println);

        System.out.println("Alle personen die minstens 30 jaar zijn en minstens 80 kg wegen: ");
        Stream.of(persons).filter(p->p.getLeeftijd()>=30).filter(p->p.getGewicht()>=80).forEach(System.out::println);

        System.out.println("Alle leeftijden van de personen: ");
        Stream.of(persons).mapToInt(p->p.getLeeftijd()).forEach(System.out::println);

        System.out.println("We sorteren alle personen op leeftijd (van jong naar oud)");
        Stream.of(persons).sorted().forEach(System.out::println);

        System.out.println("Alle voornamen en achternamen van de personen");
        Stream.of(persons).map(p->p.getVoornaam()+" "+p.getAchternaam()).forEach(System.out::println);

        System.out.println("We drukken de gemiddelde, minimale en maximale leeftijd af van al onze personen:");
        System.out.println("Gemiddelde leeftijd =  " + Stream.of(persons).mapToInt(p->p.getLeeftijd()).average().getAsDouble());
        System.out.println("Minimale leeftijd = " + Stream.of(persons).mapToInt(p->p.getLeeftijd()).min().getAsInt());
        System.out.println("Maximale leeftijd = " + Stream.of(persons).mapToInt(p->p.getLeeftijd()).max().getAsInt());

        System.out.println("Alle gewichten van de personen: ");
        Stream.of(persons).mapToDouble(p->p.getGewicht()).forEach(System.out::println);

        System.out.println("We drukken het gemiddelde, minimale en maximale gewicht af van al onze personen:");
        System.out.println("Gemiddelde gewicht =  " + Stream.of(persons).mapToDouble(p->p.getGewicht()).average().getAsDouble());
        System.out.println("Minimale gewicht = " + Stream.of(persons).mapToDouble(p->p.getGewicht()).min().getAsDouble());
        System.out.println("Maximale gewicht = " + Stream.of(persons).mapToDouble(p->p.getGewicht()).max().getAsDouble());

        System.out.println("Som van de vierkantswortels van de eerste twintig getallen tussen 0 en 1000 deelbaar door 5 en 8");

        double result = IntStream.rangeClosed(0,1000).filter(i->i%5==0&&i%8==0).limit(20)
                .mapToDouble(Double::new).reduce(0.0,(acc, el)->acc+Math.sqrt(el));
        System.out.println(result);

        System.out.println("500 willekeurige even en unieke getallen tussen 0 en 10000");
        int[] ia = new Random().ints().filter(i->i%2==0).filter(i->i>0&&i<10000).distinct().limit(500).sorted().toArray();
        for(int i: ia) System.out.println(i);




    }
}
