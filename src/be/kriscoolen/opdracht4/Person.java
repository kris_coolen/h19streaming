package be.kriscoolen.opdracht4;

public class Person implements Comparable<Person>{

   private String voornaam;
   private String achternaam;
   private Gender geslacht;
   private int leeftijd;
   private float gewicht;
   private float lengte;

    public Person(String voornaam, String achternaam, Gender geslacht, int leeftijd, float gewicht, float lengte) {
        this.voornaam = voornaam;
        this.achternaam = achternaam;
        this.geslacht = geslacht;
        this.leeftijd = leeftijd;
        this.gewicht = gewicht;
        this.lengte = lengte;
    }

    public float getGewicht() {
        return gewicht;
    }

    public float getLengte() {
        return lengte;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public Gender getGeslacht() {
        return geslacht;
    }

    public int getLeeftijd() {
        return leeftijd;
    }

    @Override
    public String toString() {
        return "\nPerson\n" +
                "\tvoornaam='" + voornaam + "\'"+
                "\n\tachternaam='" + achternaam + "\'"+
                "\n\tgeslacht=" + geslacht +
                "\n\tleeftijd=" + leeftijd + " jaar" +
                "\n\tgewicht=" + gewicht + " kilogram" +
                "\n\tlengte=" + lengte + " meter" +
                "\n";
    }

    @Override
    public int compareTo(Person p) {
       //if(this.getLeeftijd()==p.getLeeftijd()) return 0;
       //else if(this.getLeeftijd()<p.getLeeftijd()) return -1;
       //else return 1;
        return this.getLeeftijd()-p.getLeeftijd();
    }
}
